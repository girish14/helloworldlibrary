package com.khatabook.helloworld

object HelloWorld {

    fun getText(): String {
        return "This message is from Hello World library"
    }
}